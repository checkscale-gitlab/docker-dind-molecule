# Contributing

## Reporting issues

* [Open an issue](https://gitlab.com/sjugge/docker-dind-molecule/issues/new)
* Select one of the templates and complete the requested information.

## Merge requests

* [Create a merge request](https://gitlab.com/sjugge/docker-dind-molecule/merge_requests/new)
* Select a relevant template and complete the requested information.
