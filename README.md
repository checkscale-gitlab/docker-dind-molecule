# Docker image DIND Molecule

A Docker image with DIND (Docker in Docker) and Molecule for testing of Ansible roles.

## Usage

```bash
docker run -it \
  -v `pwd`:/<author>.<role> \
  -v /var/run/docker.sock:/var/run/docker.sock \
  registry.gitlab.com/sjugge/docker-dind-molecule \
  /bin/sh -c 'cd /<author>.<role> && molecule test'
```

Note: in CI setups, remove the `-it` flag.

### Registry

Refer to [this repo's registry](https://gitlab.com/sjugge/docker-dind-molecule/container_registry).

## Contributing & issues

See [CONTRIBUTING](https://gitlab.com/sjugge/docker-dind-molecule/blob/master/CONTRIBUTING.md).


## Copyright

[MIT License](https://gitlab.com/sjugge/docker-dind-molecule/blob/master/LICENSE.md)

## Author

- Jurgen Verhasselt - https://gitlab.com/sjugge
- Source: https://gitlab.com/sjugge/docker-dind-molecule